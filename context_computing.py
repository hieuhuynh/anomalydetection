"""
__author__ = 'hieu'
date = 24.11.2015
Given the probability [num_sample, num_topic] matrix, calculate the inter similarity between topics in anomaly group and
typical group. Then sort ascendingly to pick lowest-rank topics.
"""


from nltk.corpus import wordnet as wn
import numpy as np
import pandas as pd


NUM_LOWEST_TOP = 10


def similarity_computing_word(word1, word2):
    w1 = wn.synsets(str(word1))
    w2 = wn.synsets(str(word2))
    lst = []
    # for i1 in w1:
    #     for i2 in w2:
    #         t = wn.wup_similarity(i1, i2)
    #         if str(t) != 'None':
    #             lst.append(t)
    # res = np.max(np.array(lst))
    res = 0
    if len(w1) > 0 and len(w2) > 0:
        t = wn.wup_similarity(w1[0], w2[0])
        res = t if str(t)!='None' else 0
    return res


def similarity_computing(top_word_typ_file, top_word_abn_file, top_word_abnK_file):
    matrix_top_word_typical = pd.read_csv(top_word_typ_file, header=None).values
    matrix_top_word_abn_pd = pd.read_csv(top_word_abn_file, header=None, index_col = 0)
    matrix_top_word_abn = matrix_top_word_abn_pd.values
    lst_abn_score = []
    for row_abn in range(matrix_top_word_abn.shape[0]):
        abn_score = 0
        for row_typ in range(matrix_top_word_typical.shape[0]):
            for col_abn in range(matrix_top_word_abn.shape[1]):
                for col_typ in range(matrix_top_word_typical.shape[1]):
                    abn_score += similarity_computing_word(matrix_top_word_abn[row_abn, col_abn],matrix_top_word_typical[row_typ, col_typ])
        lst_abn_score.append(abn_score)
    print('-lst_abn_score: %s' % str(lst_abn_score))
    top_lowest = np.argsort(np.array(lst_abn_score))[: NUM_LOWEST_TOP]
    print('-top lowest: %s' % str(top_lowest))
    # save lowest score topic to file
    top_lowest_realIdx =  [matrix_top_word_abn_pd.index.values[idx] for idx in top_lowest]
    df = pd.DataFrame(matrix_top_word_abn[top_lowest], index = top_lowest_realIdx)
    df.to_csv(top_word_abnK_file, index=True, header=False)




