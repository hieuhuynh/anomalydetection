"""
__author__ = 'hieu'
Receive a test file, output a file containing anomalous messages
"""
import pandas as pd
from sklearn.externals import joblib
import numpy as np
import lda


def detect_anomalous_message(test_file, count_vec_model, lda_model_file, topic_anomalous_file, test_result_file):
    x_msg = pd.read_csv(test_file).message.values
    # preprocess x_msg
    # nan
    x_msg = np.array([str(x_msg[i]) for i in range(len(x_msg))])
    # stemming

    # transform to count vec
    count_vec = joblib.load(count_vec_model)
    x_count = count_vec.transform(x_msg)
    print(x_count.shape);print(type(x_count))

    # produce topics
    lda_m = joblib.load(lda_model_file)
    matrix_doc_top = lda_m.transform(x_count.toarray())
    print('- done predict topic')
    # choose topics to which docs belong
    top = [np.argmax(row_doc_top) for row_doc_top in matrix_doc_top]

    # pick anomalous messages
    topic_all_anb = pd.read_csv(topic_anomalous_file, header = None).ix[:,0].values
    idx_anb = [idx for idx in range(len(top)) if top[idx] in topic_all_anb]
    msg_anb = [x_msg[idx] for idx in idx_anb]
    top_anb = [top[idx] for idx in idx_anb]
    pd.DataFrame(np.vstack([top_anb, msg_anb]).T).to_csv(test_result_file, index= False, header= False)