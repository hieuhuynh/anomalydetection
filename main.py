"""
__author__ = 'hieu'
Main flow of the project
"""
import pandas as pd
from AnoDetect import data_crawling, lda_operations, context_computing, test_anomaly
import os


if __name__ == '__main__':
    if not os.path.exists('temp'):
        os.makedirs('temp')
    if not os.path.exists('model'):
        os.makedirs('model')
    if not os.path.exists('test_result'):
        os.makedirs('test_result')

    top_word_typ_file = 'temp/top_word_typ.csv'
    top_word_abn_file = 'temp/top_word_abn.csv'
    top_word_abnK_file = 'temp/top_word_abn_lowest.csv'

    ''' crawl data '''
    # data_crawling.data_crawl()
    print('done data crawling')

    ''' train lda '''
    train_file = 'train.csv'
    count_vec_model_file = 'model/count_vec.pkl'
    lda_model_file = 'model/lda_model.pkl'
    matrix_top_word, matrix_doc_top = lda_operations.lda_train(train_file, count_vec_model_file, lda_model_file )
    pd.DataFrame(matrix_top_word).to_csv('matrix_top_word.csv', header = False, index= False)
    pd.DataFrame(matrix_doc_top).to_csv('matrix_doc_top.csv', header = False, index= False)
    print('done lda training')
    # matrix_top_word = pd.read_csv('matrix_top_word.csv', header=None).values
    # matrix_doc_top = pd.read_csv('matrix_doc_top.csv', header=None).values
    lda_operations.rank_top(matrix_top_word,matrix_doc_top, top_word_typ_file, top_word_abn_file)

    ''' integrate context to pick abnormal topics '''
    context_computing.similarity_computing(top_word_typ_file,top_word_abn_file, top_word_abnK_file)

    ''' test '''
    test_file = 'test.csv'
    test_result_file = 'test_result/test_result.csv'
    test_anomaly.detect_anomalous_message(test_file, count_vec_model_file, lda_model_file, top_word_abnK_file, test_result_file)
