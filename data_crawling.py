'''
__author__ = 'hieu'
date = 24.11.2015
Crawl data that is the messages of HSBC within nearest months
Separate train and test set
'''
import requests
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import csv

ES_HOST = [
    {'host': 'es0.ssh.sentifi.com', 'port': 9200, 'timeout': 120 }
]
client = Elasticsearch(ES_HOST)


def query_get_msg():
    query_message_score = {
    "fields": [
    "content_s", "published_at"
    ],
    "query": {
        "filtered": {
            "filter": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "tag": "203"
                            }
                        },
                        {
                            "range": {
                                "published_at": {
                                    "from": "now-2M",
                                    "to": "now"
                                }
                            }
                        }
                    ]
                }
            }
        }
    }
    }

    return helpers.scan(client, query=query_message_score, index='rm_search_staging', doc_type='m')


def data_crawl():
    train = csv.writer(open('train.csv','w'), delimiter = ',', lineterminator= '\n')
    test = csv.writer(open('test.csv','w'), delimiter = ',', lineterminator= '\n')
    train.writerow(['message','published_at'])
    test.writerow(['message','published_at'])
    count = 0
    for hit in query_get_msg():
        msg = hit['fields']['content_s'][0]
        publish_time = hit['fields']['published_at'][0]
        if count % 4 == 0:
            test.writerow([msg, publish_time])
        else:
            train.writerow([msg, publish_time])
        count += 1




