"""
__author__ = 'hieu'
date = 24.11.2015
- train LDA model, output a probability matrix [num_doc, num_topic] and a topic-word matrix [num_topic, num_word_each_topic]
- rank topics based on KL divergence . choose a parameter m to divide topic-word matrix to M1(typical) and M2(anomaly)
"""

NUM_TOPIC = 100
NUM_WORD_EACH_TOPIC = 30
M = 0.3
N_ITER = 1000
SEED = 2015
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.manifold import MDS
from sklearn.externals import joblib
import lda
import numpy as np


def lda_train(file_train, count_vec_model_file, lda_model_file):
    # read train data
    x_msg = pd.read_csv(file_train).message.values
    # preprocess x_msg
    # nan
    x_msg = np.array([str(x_msg[i]) for i in range(len(x_msg))])
    # stemming

    # convert to bag_of_words matrix
    model_count_vec = CountVectorizer(stop_words=None, lowercase=True, ngram_range=(1,1), min_df = 3)
    x_bag = model_count_vec.fit_transform(x_msg)
    vocab = np.array(model_count_vec.get_feature_names())
    assert len(vocab) == x_bag.shape[1], 'len of vocab mismatch: %d,%d' % (len(vocab), x_bag.shape[1])
    joblib.dump(model_count_vec, count_vec_model_file)
    # train lda
    lda_model = lda.LDA(n_topics=NUM_TOPIC, n_iter=N_ITER, random_state=SEED)
    lda_model.fit(x_bag)
    joblib.dump(lda_model, lda_model_file)
    # produce two matrix
    matrix_doc_top = lda_model.doc_topic_
    print('- matrix_doc_top'); print(matrix_doc_top[:3,:])
    matrix_top_word = []
    component = lda_model.components_
    print(component[0])
    for idx in range(NUM_TOPIC):
        matrix_top_word.append(list(vocab[np.argsort(component[idx])][: - (NUM_WORD_EACH_TOPIC + 1) : -1]))
    matrix_top_word = np.vstack(matrix_top_word)
    print('- matrix_top_word'); print(matrix_top_word[: 3, :])
    return matrix_top_word, matrix_doc_top


def KL_comp_optimize(matrix_doc_top, top_1, top_2):
     p1 = matrix_doc_top[:, top_1]
     p2 = matrix_doc_top[:, top_2]
     kl = np.dot(p1,np.log(p1/p2))
     return kl


def rank_top(matrix_top_word, matrix_doc_top, top_word_typ_file, top_word_abn_file):
    """
    calculate symmetrized KL-divergence distance between topics. Do a multidimensional scaling and use the first dimension
    to order topics
    """
    matrix_KL = np.zeros((NUM_TOPIC, NUM_TOPIC))
    for row_id_1 in range(NUM_TOPIC):
        for row_id_2 in range(row_id_1 + 1, NUM_TOPIC):
            KL_sym = KL_comp_optimize(matrix_doc_top, row_id_1, row_id_2) + KL_comp_optimize(matrix_doc_top, row_id_2, row_id_1)
            matrix_KL[row_id_1, row_id_2] = KL_sym
            matrix_KL[row_id_2, row_id_1] = KL_sym
    print('- done export matrix_KL distance')
    # multidimensional scaling
    mds = MDS(n_components=2, metric= True, max_iter=3000, eps=1e-9, random_state=SEED,dissimilarity="precomputed")
    pos = mds.fit(matrix_KL).embedding_
    print('- pos shape : %s' % str(pos.shape))
    # rank and split topics into typical/anomaly groups
    num_abn = int(NUM_TOPIC * M)
    num_typ = NUM_TOPIC - num_abn
    top_abn = np.argsort(pos[:,0])[: num_abn]
    top_typ = [i for i in range(NUM_TOPIC) if i not in top_abn]

    # produce two matrix
    df = pd.DataFrame(matrix_top_word[top_typ,:])
    df.to_csv(top_word_typ_file, header= False, index= False)
    df = pd.DataFrame(matrix_top_word[top_abn, :],index= top_abn)
    df.to_csv(top_word_abn_file, header=False, index= True)









